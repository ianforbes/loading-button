(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
    typeof define === 'function' && define.amd ? define(['exports'], factory) :
    (factory((global.LoadingButton = {})));
}(this, (function (exports) { 'use strict';

    (function(){ if(typeof document !== 'undefined'){ var head=document.head||document.getElementsByTagName('head')[0], style=document.createElement('style'), css=""; style.type='text/css'; if (style.styleSheet){ style.styleSheet.cssText = css; } else { style.appendChild(document.createTextNode(css)); } head.appendChild(style); } })();












    var component = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('button',{on:{"click":function($event){_vm.enterLoadingState();}}},[(_vm.isLoading)?[_c('i',{class:_vm.loadingIcon}),_vm._v(" "+_vm._s(_vm.loadingText)+"... ")]:[_vm._t("default")]],2)},staticRenderFns: [],
        props: {
            loadingIcon: {
                type: String,
                default: "fas fa-circle-notch fa-spin",
            },
            loadingText: {
                type: String,
                default: "Loading",
            },
        },

        data: function data() {
            return {
                isLoading: false,
            }
        },

        methods: {
            enterLoadingState: function enterLoadingState() {
                this.isLoading = true;
            },
        },
    }

    // Import vue component

    // Declare install function executed by Vue.use()
    function install(Vue) {
    	if (install.installed) { return; }
    	install.installed = true;
    	Vue.component('LoadingButton', component);
    }

    // Create module definition for Vue.use()
    var plugin = {
    	install: install,
    };

    // Auto-install when vue is found (eg. in browser via <script> tag)
    var GlobalVue = null;
    if (typeof window !== 'undefined') {
    	GlobalVue = window.Vue;
    } else if (typeof global !== 'undefined') {
    	GlobalVue = global.Vue;
    }
    if (GlobalVue) {
    	GlobalVue.use(plugin);
    }

    exports.install = install;
    exports.default = component;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
