# Customisable Vue Loading Button Component #

This repo contains a simple Vue component: a button that when clicked has a customisable loading state.

### Installation ###

To install the package using npm:
```
npm i @ianfusion/vue-loading-button
```
Then register the component:
```
// In your app.js or similar file
import VueLoadingButton from '@ianfusion/vue-loading-button';

var vm = new Vue({
    el: '#app',
    components: {
        VueLoadingButton,
    },
});
```

### Usage ###

Here is an example below:
```html
<vue-loading-button loading-icon="fa fa-cog fa-spin" loading-text="loading text">normal text</vue-loading-button>
```
This will make a button with the text "normal text", and when clicked on has text "loading text" and an icon of a spinning cog.

![Example](loading-button-example.gif)

The two attributes are:
* ```loading-text``` - Where you can put the text you want to show after being clicked.
* ```loading-icon``` - Where you can put an icon that you want to show after being clicked. (More available at [Fontawesome](https://fontawesome.com/))


### Support ###

If you come across any issues feel free to email me: ian@infusion-it.co.uk.

### License ###

The MIT License (MIT). Please see the [License File](LICENSE) for more information.
